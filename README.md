# Simple moddable example Godot project

A boring showcase of basic modding implemented in Godot.
Test it with the example mods [here](https://gitlab.com/Zatherz/godot-simple-moddable-example-mods).
