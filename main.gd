extends Node

onready var labels = get_node("Labels")

func _ready():
	for key in ModEngine.mods.keys():
		var mod = ModEngine.mods[key]
		print("USE MOD: ", key)
		labels.add_child(mod.instance())