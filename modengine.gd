extends Node

const MODS_DIR = "user://mods/"
var mods = {}

func _ready():
	var dir = Directory.new()
	if not dir.dir_exists(ModEngine.MODS_DIR):
		print("mods folder doesn't exist")
		return
	dir.open(ModEngine.MODS_DIR)
	dir.list_dir_begin()
	var mod = dir.get_next()
	while (mod != ""):
		if mod == "." or mod == "..":
			mod = dir.get_next()
			continue
		if dir.current_is_dir():
			print("ADD MOD: " + mod)
			ModEngine.add_mod(mod)
		mod = dir.get_next()

func add_mod(name):
	var scene = load(str(MODS_DIR, name, "/label.tscn"))
	if scene == null:
		print("Failed loading mod ", name)
	else:
		mods[name] = scene